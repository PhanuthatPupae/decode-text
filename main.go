package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func main() {
	// result := DecodeText("LLRR=")
	// [0 0 0 1 0 -1]
	// fmt.Println(result)
	input := []string{
		"LLRR=",
		// "==RLL",
		// "=LLRR",
		// "RRL=R",
	}
	for _, i := range input {
		result1 := DecodeText(i)
		fmt.Println(result1)
	}

}

func DecodeText(text string) string {

	minValue := 0

	words := strings.Split(text, "")
	numbers := []int{0, 1, 2, 3, 4, 5}

	for _, n := range numbers {
		for _, n2 := range numbers {
			lastResult := n2
			initValue := []int{}
			initValue = append(initValue, n)
			//TODO find min result
			for _, w := range words {
				if strings.ToUpper(w) == "L" {
					lastResult = lastResult - 1
				} else if strings.ToUpper(w) == "R" {

					lastResult = lastResult + 1
				}

				initValue = append(initValue, lastResult)
				if lastResult < minValue {
					minValue = lastResult
				}

			}
			fmt.Println("initValue", initValue)

			result := make([]string, len(initValue))
			// for idx, w := range words {
			// 	if w == "=" && initValue[idx] == initValue[idx+1] {
			// 		result = append(result, strconv.Itoa(initValue[idx]))
			// 	} else {
			// 		result = append(result, strconv.Itoa(int(math.Abs(float64(minValue)))+initValue[idx]))
			// 	}
			// }

			for idx, value := range initValue {
				if idx > len(words)-1 {
					result = append(result, strconv.Itoa(int(math.Abs(float64(minValue)))+value))
				} else {
					fmt.Println(words[idx])
					if words[idx] == "=" && initValue[idx] == initValue[idx+1] {
						result[idx] = strconv.Itoa(initValue[idx])
						result[idx+1] = strconv.Itoa(initValue[idx])

					} else if words[idx] == "L" {
						result[idx] = strconv.Itoa(int(math.Abs(float64(minValue))) + initValue[idx])
						// result = append(result, strconv.Itoa(int(math.Abs(float64(minValue)))+initValue[idx]))
					} else if words[idx] == "R" {
						result[idx] = strconv.Itoa(int(math.Abs(float64(minValue))) + initValue[idx])
						// result = append(result, strconv.Itoa(int(math.Abs(float64(minValue)))+initValue[idx]))
					}

				}

				//convert value to positive by max value because result value is not nagative
				// result = append(result, strconv.Itoa(int(math.Abs(float64(minValue)))+value))
			}
			fmt.Println("result ===>", strings.Join(result, ""))
		}

	}

	return strings.Join([]string{}, "")
}
