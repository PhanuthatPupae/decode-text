package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type DecodeTextInput struct {
	Input  string
	Expect string
}

func TestDecodeText(t *testing.T) {
	testCase := []DecodeTextInput{
		{Input: "LLRR=", Expect: "210122"},
		// {Input: "==RLL", Expect: "000210"},
		{Input: "=LLRR", Expect: "221012"},
		// {Input: "RRL=R", Expect: "012001"},

	}
	for _, tc := range testCase {
		result := DecodeText(tc.Input)
		assert.Equal(t, tc.Expect, result)

	}

}
